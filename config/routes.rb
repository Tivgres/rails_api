Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      post 'token', to: 'tokens#create'
      delete 'token', to: 'tokens#destroy'
      post 'register', to: 'users#create'
      get 'time', to: 'time#time'
    end
  end
end
