class JsonWebToken
  def self.encode(payload)
    payload.reverse_merge!(meta)
    JWT.encode(payload, ENV['SECRET_KEY_BASE'])
  end

  def self.decode(token)
    JWT.decode(token, ENV['SECRET_KEY_BASE'])
  end

  def self.valid_payload(payload)
    if expired(payload) || payload['iss'] != meta[:iss] || payload['aud'] != meta[:aud]
      return false
    else
      return true
    end
  end

  def self.meta
    {
      exp: 10.years.from_now.to_i,
      iss: ENV['ISS_TOKEN'],
      aud: ENV['AUD_TOKEN'],
    }
  end

  def self.expired(payload)
    Time.at(payload['exp']) < Time.now
  end
end