json.set! :user do
  if @user.errors.any?
    json.extract! @user, :errors
  else
    json.extract! @user, :id, :email, :username, :first_name, :last_name, :created_at
  end
end