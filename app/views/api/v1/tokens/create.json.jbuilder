json.set! :token do
  if @token.errors.any?
    json.extract! @token, :errors
  else
    json.extract! @token, :token, :created_at
    json.set! :assigned_to do
      json.extract! @token.user, :email, :username
    end
  end
end