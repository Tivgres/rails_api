class User < ApplicationRecord
  has_secure_password

  has_many :tokens, dependent: :delete_all

  validates :email, :password, :username, :first_name, :last_name, presence: true
  validates :password, :username, length: { minimum: 6 }
  validates :username, length: { maximum: 50 }
  validates :email, format: /@/
  validates :username, :email, uniqueness: { case_sensitive: false }
end
