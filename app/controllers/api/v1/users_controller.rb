class Api::V1::UsersController < ApplicationController

  def create
    @user = User.new user_params
    if @user.save
      render status: :ok
    else
      render status: :bad_request
    end
  end

  private

  def user_params
    params.require(:user).permit %i[email password username first_name last_name]
  end
end
