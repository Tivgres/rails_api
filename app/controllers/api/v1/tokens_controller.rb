require 'json_web_token'

class Api::V1::TokensController < ApplicationController

  before_action :authenticate_request!

  def create
    @token = @current_user.tokens.new(token: JsonWebToken.encode({ user_id: @current_user.id }))
    if @token.save
      render status: :ok
    else
      render status: :bad_request
    end
  end

  def destroy
    @token = @current_user.tokens.find_by(token: token_params[:token])
    if @token&.destroy
      @deleted = { deleted: @token&.destroyed?, cause: :success }
      render status: :ok
    else
      @deleted = { deleted: @token&.destroyed? || false, cause: @token&.errors || 'Does not exist' }
      render status: :bad_request
    end
  end

  private

  def token_params
    params.require(:token).permit(:token)
  end
end
