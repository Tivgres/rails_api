class Api::V1::TimeController < ApplicationController
  before_action :authenticate_request!

  def time
    render json: { time: Time.now }, status: :ok
  end
end