require 'base64'

class ApplicationController < ActionController::API

  protected

  def authenticate_request!
    @current_user = authorize
    invalid_authentication unless @current_user
  end

  def invalid_authentication
    render json: { error: 'Invalid credentials' }, status: :unauthorized
  end

  private

  def authorize
    auth_header = request.headers['Authorization']
    return nil unless auth_header
    auth_header.include?('Basic') ? basic_auth(auth_header) : token_auth(auth_header)
  rescue
    nil
  end

  def basic_auth(auth_body)
    auth_body = auth_body.split.last
    user_credentials = Base64.decode64(auth_body).split(':')
    User.find_by(email: user_credentials[0])&.authenticate(user_credentials[1])
  end

  def token_auth(auth_body)
    token = JsonWebToken.decode(auth_body)
    return nil unless JsonWebToken.valid_payload(token.first)
    user = User.find_by(id: token[0]['user_id'])
    return nil unless user&.tokens&.find_by(token: auth_body)
    user
  end
end
