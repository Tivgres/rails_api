# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

    ```2.5.1```

* Database creation

    ```rake db:create```

* Database initialization

    ```rake db:migrate```

* How to run the test suite

    ```rspec /spec```

**API documentation**

* Registration

    * url
        
        ```
        /api/v1/register
        ```

    * method
    
        ```
        POST
        ```
    
    * headers
    
        ```
        Content-Type: application/json
        ```
    
    * request body
    
        ```
        {
            "user": {
                "email": "email@example.com",
                "password": "some_secret_password",
                "username": "some_username",
                "first_name": "some_first_name",
                "last_name": "some_last_name"
            }
        }
        ```
        
        Email must be valid.
        
        Password must contain more than 6 characters and less than 72 characters.
        
        Username must contain more than 6 characters and less than 50 characters.
        
        First name must be present.
        
        Last name must be present.
    
    * success response body
        
        ```
        {
            "user": {
                "id": 15,
                "email": "email@example.com",
                "username": "some_username",
                "first_name": "some_first_name",
                "last_name": "some_last_name",
                "created_at": "2018-07-20T08:24:30.081Z"
            }
        }
        ```
        
    * fails inside response body
    
        ```
        {
            "user": {
                "errors": {
                    "first_name": [
                        "can't be blank"
                    ],
                    "last_name": [
                        "can't be blank"
                    ],
                    "password": [
                        "is too short (minimum is 6 characters)"
                    ],
                    "username": [
                        "has already been taken"
                    ],
                    "email": [
                        "has already been taken"
                    ]
                }
            }
        }
        ```
        
* Obtain the token

    * url
            
        ```
        /api/v1/token
        ```

    * method
    
        ```
        POST
        ```

    * Basic auth
    
        * headers
        
            ```
            Content-Type: application/json
            ```
        
            ```
            Authorization: Basic ZW1haWxAZXhhbXBsZS5jb206cGFzc3dvcmQ=
            ```
            
    * Token auth
        
        * headers
        
            ```
            Content-Type: application/json
            ```
        
            ```
            Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE4NDc2OTg3MjEsImlzcyI6IkFQSV9BUFAiLCJhdWQiOiJDTElFTlQiLCJ1c2VyX2lkIjoxNX0.uJsRoNHLkCYAkj6zzk9J4wYde9WfxnVIsNdErZo7npc
            ```
    
    * success response body
        
        ```
        {
            "token": {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE4NDc2OTg3MjEsImlzcyI6IkFQSV9BUFAiLCJhdWQiOiJDTElFTlQiLCJ1c2VyX2lkIjoxNX0.uJsRoNHLkCYAkj6zzk9J4wYde9WfxnVIsNdErZo7npc",
                "created_at": "2018-07-20T09:38:41.619Z",
                "assigned_to": {
                    "email": "email@example.com",
                    "username": "user_username"
                }
            }
        }
        ```
            
    * fails inside response body
        
        ```
        {
            "error": "Invalid credentials"
        }
        ```    
            
* Delete the token

    * url
            
        ```
        /api/v1/token
        ```

    * method
    
        ```
        DELETE
        ```
        
    * Basic auth
    
        * headers
        
            ```
            Content-Type: application/json
            ```
        
            ```
            Authorization: Basic ZW1haWxAZXhhbXBsZS5jb206cGFzc3dvcmQ=
            ```
               
    * Token auth
        
        * headers
        
            ```
            Content-Type: application/json
            ```
        
            ```
            Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE4NDc2OTg3MjEsImlzcyI6IkFQSV9BUFAiLCJhdWQiOiJDTElFTlQiLCJ1c2VyX2lkIjoxNX0.uJsRoNHLkCYAkj6zzk9J4wYde9WfxnVIsNdErZo7npc
            ```
    
    * request body
        
        ```
        {
            "token": {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE4NDc2OTg3MjEsImlzcyI6IkFQSV9BUFAiLCJhdWQiOiJDTElFTlQiLCJ1c2VyX2lkIjoxNX0.uJsRoNHLkCYAkj6zzk9J4wYde9WfxnVIsNdErZo7npc"
            }
        }
        ```
    
    * success response body
        
        ```
        {
            "token": {
                "deleted": true,
                "cause": "success"
            }
        }
        ```
            
    * fails inside response body
    
        ```
        {
            "token": {
                "deleted": false,
                "cause": "Does not exist"
            }
        }
        ```
        
        **OR when you used invalid credentials:**
        
        ```
        {
            "error": "Invalid credentials"
        }
        ```