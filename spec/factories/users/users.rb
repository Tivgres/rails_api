require 'faker'

FactoryBot.define do
  factory :user do

    factory :user_without_email, parent: :user do
      # here is no email
      password { Faker::Internet.password }
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_with_wrong_email, parent: :user do
      email 'this_is_email.without.sign'
      password { Faker::Internet.password }
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_without_password, parent: :user do
      email { Faker::Internet.email }
      # here is no password
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_with_wrong_password_less, parent: :user do
      email { Faker::Internet.email }
      password '1122'
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_with_wrong_password_over, parent: :user do
      email { Faker::Internet.email }
      password '1' * 100
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_without_username, parent: :user do
      email { Faker::Internet.email }
      password {Faker::Internet.password }
      # here is no username
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_with_wrong_username_less, parent: :user do
      email { Faker::Internet.email }
      password {Faker::Internet.password }
      username 'admin'
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_with_wrong_username_over, parent: :user do
      email { Faker::Internet.email }
      password {Faker::Internet.password }
      username 'admin' * 15
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end

    factory :user_without_first_name, parent: :user do
      email { Faker::Internet.email }
      password { Faker::Internet.password }
      username { Faker::Internet.user_name(10..20) }
      # here is no first_name
      last_name { Faker::Name.last_name }
    end

    factory :user_without_last_name, parent: :user do
      email { Faker::Internet.email }
      password { Faker::Internet.password }
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      # here is no last_name
    end

    factory :user_with_all_important, parent: :user do
      email { Faker::Internet.email }
      password { Faker::Internet.password }
      username { Faker::Internet.user_name(10..20) }
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end
  end
end