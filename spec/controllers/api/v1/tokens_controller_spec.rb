require 'rails_helper'
require 'base64'
require 'json'

RSpec.describe Api::V1::TokensController, type: :controller do
  render_views

  # variables for a tests with valid params
  let (:my_email) { 'mr.serg.vit@gmail.com' }
  let (:my_pass) { '44332211' }
  let (:invalid_token) { 'Q2hlY2sgdGhlIHdlYnNpdGUgaHR0cHM6Ly90aXZncmVzLm1l' }
  let (:invalid_basic) { "Basic #{invalid_token}" }
  let (:valid_basic) { "Basic #{Base64.encode64("#{my_email}:#{my_pass}")}" }
  let (:deleted_token) { 'this variable will be used in tests' }

  before do
    request.headers['HTTP_ACCEPT'] = 'application/json; charset=utf-8'
  end

  before(:context) do
    create(:user_with_all_important,
           email: 'mr.serg.vit@gmail.com',
           password: '44332211')
  end

  after(:context) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  describe 'POST #create' do
    context 'with invalid params' do
      it 'should be error with invalid basic credentials' do
        request.headers['Authorization'] = invalid_basic
        expect {
          post :create
        }.to have_http_status(401) && change(Token, :count).by(0)
      end

      it 'should be error with invalid token credentials' do
        request.headers['Authorization'] = invalid_token
        expect {
          post :create
        }.to have_http_status(401) && change(Token, :count).by(0)
      end
    end

    context 'with valid params' do
      it 'should be success with basic credentials' do
        request.headers['Authorization'] = valid_basic
        post :create
        expect(response).to have_http_status(200)
        change(Token, :count).by(1)
        expect(response).to render_template('api/v1/tokens/create')
        expect(response.body.present?).to be_truthy
      end

      it 'should be success with token credentials' do
        request.headers['Authorization'] = Token.last.token
        post :create
        expect(response).to have_http_status(200)
      end

    end
  end

  describe 'DELETE #destroy' do
    context 'with with valid params' do
      it 'should be success with valid basic credentials & valid token' do
        obtain_token
        request.headers['Authorization'] = valid_basic
        delete :destroy, params: { token: { token: Token.last.token } }
        expect(response).to have_http_status(200)
      end

      it 'should be success with valid token credentials & valid token' do
        obtain_token
        request.headers['Authorization'] = Token.last.token
        delete :destroy, params: { token: { token: Token.last.token } }
        expect(response).to have_http_status(200)
      end
    end

    context 'with with valid params' do
      it 'should be error with valid basic credentials & invalid token' do
        request.headers['Authorization'] = valid_basic
        delete :destroy, params: { token: { token: invalid_token } }
        expect(response).to have_http_status(400)
      end

      it 'should be error with invalid basic credentials & invalid token' do
        request.headers['Authorization'] = invalid_basic
        delete :destroy, params: { token: { token: invalid_token } }
        expect(response).to have_http_status(401)
      end

      it 'should be error with valid token credentials & invalid token' do
        obtain_token
        request.headers['Authorization'] = Token.last.token
        delete :destroy, params: { token: { token: invalid_token } }
        expect(response).to have_http_status(400)
      end

      it 'should be error with invalid token credentials & invalid token' do
        obtain_token
        request.headers['Authorization'] = invalid_token
        delete :destroy, params: { token: { token: invalid_token } }
        expect(response).to have_http_status(401)
      end

      it 'should be error with valid basic credentials & deleted token' do
        request.headers['Authorization'] = valid_basic
        deleted_token = Token.last.token
        Token.all.destroy_all
        delete :destroy, params: { token: { token: deleted_token } }
        expect(response).to have_http_status(400)
        expect(response.body).to include('false')
      end

      it 'should be error with valid token credentials & deleted token' do
        obtain_token
        request.headers['Authorization'] = Token.last.token
        delete :destroy, params: { token: { token: deleted_token } }
        expect(response).to have_http_status(400)
        expect(response.body).to include('false')
      end

    end
  end

  # This is like a helper for obtain new token
  def obtain_token
    request.headers['Authorization'] = valid_basic
    post :create
  end
end
