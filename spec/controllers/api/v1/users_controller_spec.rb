require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do

  # variables for a tests with valid params but invalid situations
  let (:my_email) { 'mr.serg.vit@gmail.com' }
  let (:my_pass) { '44332211' }

  before do
    request.headers['HTTP_ACCEPT'] = 'application/json; charset=utf-8'
  end

  after(:context) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  describe 'POST #create' do

    context 'with invalid email params' do
      it 'should be error with empty email' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_without_email) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end

      it 'should be error with wrong email' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_with_wrong_email) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end
    end

    context 'with invalid password params' do
      it 'should be error with empty password' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_without_password) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end

      it 'should be error with less password' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_with_wrong_password_less) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end

      it 'should be error with over password' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_with_wrong_password_over) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end
    end

    context 'with invalid username params' do
      it 'should be error with empty username' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_without_username) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end

      it 'should be error with less username' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_with_wrong_username_less) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end

      it 'should be error with over username' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_with_wrong_username_over) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end
    end

    context 'with invalid first_name params' do
      it 'should be error without first_name' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_without_first_name) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end
    end

    context 'with invalid last_name params' do
      it 'should be error without last_name' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_without_last_name) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end
    end

    context 'with valid params' do
      it 'should be valid' do
        expect {
          post :create, params: { user: FactoryBot.attributes_for(
              :user_with_all_important,
              email: my_email,
              password: my_pass) }
        }.to have_http_status(200) && change(User, :count).by(1)
      end

      it 'should be invalid because duplicate' do
      expect {
        post :create, params: { user: FactoryBot.attributes_for(
          :user_with_all_important,
          email: my_email,
          password: my_pass) }
        }.to have_http_status(400) && change(User, :count).by(0)
      end
    end

  end
end