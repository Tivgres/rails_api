class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name,                 null: false, default: ''
      t.string :last_name,                  null: false, default: ''
      t.string :username,                   null: false, unique: true, default: ''
      t.string :email,                      null: false, unique: true, default: ''
      t.string :password_digest,            null: false, default: ''
      t.timestamps
    end
    add_index :users, :first_name
    add_index :users, :last_name
    add_index :users, :email,                unique: true
    add_index :users, :username,             unique: true
    add_index :users, :password_digest
  end
end
